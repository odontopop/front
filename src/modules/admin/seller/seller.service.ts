import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
   providedIn: 'root',
})
export class SellerService {
   constructor(private http: HttpClient) {}

   getManySeller(): any {
      return this.http.get(`${environment.baseURL}/subsidiary`).toPromise();
   }

   save(seller: any) {
      if (seller.id === null) {
         delete seller.id;
         return this.http
            .post(`${environment.baseURL}/subsidiary`, seller)
            .toPromise();
      } else {
         return this.http
            .put(`${environment.baseURL}/subsidiary`, seller)
            .toPromise();
      }
   }
}
