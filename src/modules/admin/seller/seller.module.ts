import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminSellerComponent } from './seller.component';
import { CommonModules } from '../../../shared/modules/common-modules/common-modules.module';
import { SellerService } from './seller.service';
import { PlanService } from '../plan/plan.service';
import { PlanModalSaveModule } from 'src/components/admin/plan-modal-save/plan-modal-save.module';

@NgModule({
   declarations: [AdminSellerComponent],
   imports: [CommonModule, CommonModules, PlanModalSaveModule],
   providers: [SellerService, PlanService],
})
export class AdminSellerModule {}
