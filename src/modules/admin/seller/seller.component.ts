import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {faEdit, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {MessagesService} from '../../../shared/services/messages.service';
import {FormsService} from '../../../shared/services/forms.service';
import {PlanService} from '../plan/plan.service';
import {PlanModalSaveComponent} from 'src/components/admin/plan-modal-save/plan.modal-save.component';
import {SellerService} from './seller.service';

@Component({
   selector: 'app-admin-seller',
   templateUrl: './seller.component.html',
   styleUrls: ['./seller.component.scss'],
})
export class AdminSellerComponent implements OnInit {
   @ViewChild('planModalSave') planModalSave: PlanModalSaveComponent;

   isLoading = false;
   faEdit = faEdit;
   faPlus = faPlusCircle;

   sellerModalRef: NgbModalRef;
   sellerFormGroup: FormGroup;
   sellers: any[] = [];

   plans: any[] = [];

   constructor(
      private sellerService: SellerService,
      private planService: PlanService,
      private modalService: NgbModal,
      private fb: FormBuilder,
      private msgService: MessagesService,
      private formsService: FormsService
   ) {
   }

   openModalSeller(modalContent, seller = null) {
      this.sellerFormGroup = this.fb.group({
         id: this.fb.control(seller ? seller.id : null),
         name: this.fb.control(
            seller ? seller.name : null,
            Validators.required
         ),
         cnpj: this.fb.control(
            seller ? seller.cnpj : null,
            Validators.required
         ),
         active: this.fb.control(seller ? seller.active : true),
         plan: this.builderSellerPlanFormArray(seller ? seller.plan : []),
      });

      this.sellerModalRef = this.modalService.open(modalContent);
   }

   builderSellerPlanFormArray(sellerPlans = []) {
      return this.fb.array(
         this.plans.map((plan: any) => {
            const checked = sellerPlans.filter(
               (sellerPlan) => sellerPlan.id === plan.id
            );
            return this.fb.group({
               id: this.fb.control(plan ? plan.id : null),
               name: this.fb.control(plan ? plan.name : null),
               checked: this.fb.control(checked.length > 0),
            });
         })
      );
   }

   async save() {
      if (this.sellerFormGroup.invalid) {
         this.formsService.validateAllFormFields(this.sellerFormGroup);
         this.msgService.validateForm();
      } else {
         try {
            const payload = {
               ...this.sellerFormGroup.value,
               plan: this.sellerFormGroup
                  .get('plan')
                  .value.filter((plan) => plan.checked),
            };

            if (payload.plan.length === 0) {
               this.msgService.error('Selecione pelo menos um plano');
            } else {
               this.isLoading = true;
               await this.sellerService.save(payload);
               this.getManySeller();
               this.msgService.success();
               this.sellerModalRef.close();
            }
         } catch (err) {
            this.msgService.error();
         } finally {
            this.isLoading = false;
         }
      }
   }

   async getManyPlan() {
      try {
         const {data} = await this.planService.getManyPlan();
         this.plans = data;
         this.builderSellerPlanFormArray([]);
      } catch (err) {
         this.msgService.error();
      } finally {
         this.isLoading = false;
      }
   }

   async getManySeller() {
      try {
         const {data} = await this.sellerService.getManySeller();
         this.sellers = data;
      } catch (err) {
         this.msgService.error();
      } finally {
         this.isLoading = false;
      }
   }

   openModalPlan(plan = null) {
      this.planModalSave.openModalPlan(plan);
   }

   callBackOnSavePlan(success) {
      if (success) {
         this.getManyPlan();
      }
   }

   ngOnInit() {
      this.getManySeller();
      this.getManyPlan();
   }
}
