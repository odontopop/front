import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AdminComponent} from './admin.component';
import {AdminPlanComponent} from './plan/plan.component';
import {AdminSellerComponent} from './seller/seller.component';
import {AdminUserComponent} from './user/user.component';
import {AdminContractComponent} from './contract/contract.component';
import {AdminConfigurationComponent} from './configuration/configuration.component';

const routes: Routes = [
   {
      path: '',
      component: AdminComponent,
      children: [
         {
            path: 'programas',
            component: AdminPlanComponent,
         },
         {
            path: 'clinicas',
            component: AdminSellerComponent,
         },
         {
            path: 'usuarios',
            component: AdminUserComponent,
         },
         {
            path: 'contratos',
            component: AdminContractComponent,
         },
         {
            path: 'configuracoes',
            component: AdminConfigurationComponent,
         },
      ]
   },
];

@NgModule({
   imports: [RouterModule.forChild(routes)],
   exports: [RouterModule]
})
export class AdminRoutingModule {
}
