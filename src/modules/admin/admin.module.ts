import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

import { AdminRoutingModule } from './admin.routing.module';
import { AdminComponent } from './admin.component';
import { AdminPlanModule } from './plan/plan.module';
import { AdminNavbarModule } from '../../components/admin/navbar/navbar.module';
import { AdminSellerModule } from './seller/seller.module';
import { AdminUserModule } from './user/user.module';
import { AdminContractModule } from './contract/contract.module';
import { AdminConfigurationModule } from './configuration/configuration.module';
import { PlanModalSaveModule } from 'src/components/admin/plan-modal-save/plan-modal-save.module';

@NgModule({
   declarations: [AdminComponent],
   imports: [
      CommonModule,
      NgbCollapseModule,

      AdminNavbarModule,
      AdminPlanModule,
      AdminSellerModule,
      AdminUserModule,
      AdminContractModule,
      AdminConfigurationModule,
      PlanModalSaveModule,
      AdminRoutingModule,
   ],
   providers: [],
})
export class AdminModule {}
