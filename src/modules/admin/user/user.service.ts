import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
   providedIn: 'root',
})
export class UserService {
   constructor(private http: HttpClient) {}

   getManyUser(): any {
      return this.http.get(`${environment.baseURL}/users`).toPromise();
   }

   save(user: any) {
      if (user.id === null) {
         delete user.id;
         return this.http
            .post(`${environment.baseURL}/users/user`, user)
            .toPromise();
      } else {
         delete user.password;
         return this.http
            .put(`${environment.baseURL}/users/user`, user)
            .toPromise();
      }
   }
}
