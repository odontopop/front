import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {faEdit, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {MessagesService} from '../../../shared/services/messages.service';
import {FormsService} from '../../../shared/services/forms.service';
import {UserService} from './user.service';
import {SellerService} from '../seller/seller.service';
import {Subscription} from 'rxjs';

@Component({
   selector: 'app-admin-user',
   templateUrl: './user.component.html',
   styleUrls: ['./user.component.scss'],
})
export class AdminUserComponent implements OnInit, OnDestroy {
   isLoading = false;
   faEdit = faEdit;
   faPlus = faPlusCircle;

   userModalRef: NgbModalRef;
   userFormGroup: FormGroup;
   users: any[] = [];
   sellers = [];
   genders = [
      {
         name: 'Masculino',
         value: 'MALE',
      },
      {
         name: 'Feminino',
         value: 'FEMALE',
      },
   ];
   profiles = [
      {
         name: 'Administrador',
         value: 'ADMIN',
      },
      {
         name: 'Usuário',
         value: 'USER',
      },
   ];
   profileChanges$: Subscription;

   constructor(
      private userService: UserService,
      private sellerService: SellerService,
      private modalService: NgbModal,
      private fb: FormBuilder,
      private msgService: MessagesService,
      private formsService: FormsService
   ) {
   }

   openModalUser(modalContent, user = null) {
      this.userFormGroup = this.fb.group({
         id: this.fb.control(user ? user.id : null),
         name: this.fb.control(user ? user.name : null, Validators.required),
         nickname: this.fb.control(
            user ? user.nickname : null,
            Validators.required
         ),
         mail: this.fb.control(user ? user.mail : null, Validators.required),
         phone: this.fb.control(user ? user.phone : null, Validators.required),
         gender: this.fb.control(
            user ? user.gender : null,
            Validators.required
         ),
         password: this.fb.control(
            user ? user.password : null,
            Validators.compose(user ? [] : [Validators.required])
         ),
         cpf: this.fb.control(user ? user.cpf : null),
         profile: this.fb.control(
            user ? user.profile : '',
            Validators.required
         ),
         status: this.fb.control(user ? user.status?.toUpperCase() === 'ENABLED' : true),
         subsidiary: this.fb.control(user ? user.subsidiary?.id : null),
      });

      this.userModalRef = this.modalService.open(modalContent);
      this.setProfileValidators();
   }

   async getManyUser() {
      try {
         const {data} = await this.userService.getManyUser();
         this.users = data;
      } catch (err) {
         this.msgService.error();
      } finally {
         this.isLoading = false;
      }
   }

   async getManySeller() {
      try {
         const {data} = await this.sellerService.getManySeller();
         this.sellers = data;
      } catch (err) {
         this.msgService.error();
      } finally {
         this.isLoading = false;
      }
   }

   async save() {
      if (this.userFormGroup.invalid) {
         console.log(this.userFormGroup);
         this.formsService.validateAllFormFields(this.userFormGroup);
         this.msgService.validateForm();
      } else {
         try {
            this.isLoading = true;
            await this.userService.save({
               ...this.userFormGroup.value,
               status: this.userFormGroup.get('status').value ? 'ENABLED' : 'DISABLED',
            });
            this.getManyUser();
            this.msgService.success();
            this.userModalRef.close();
         } catch (err) {
            this.msgService.error();
         } finally {
            this.isLoading = false;
         }
      }
   }

   setProfileValidators() {
      const subsidiaryControl = this.userFormGroup.get('subsidiary');
      this.profileChanges$ = this.userFormGroup
         .get('profile')
         .valueChanges.subscribe((profile) => {
            if (profile === 'ADMIN') {
               subsidiaryControl.setValidators(null);
            } else {
               subsidiaryControl.setValidators([Validators.required]);
            }
            subsidiaryControl.updateValueAndValidity();
         });
   }

   ngOnInit() {
      this.getManyUser();
      this.getManySeller();
   }

   ngOnDestroy() {
      this.profileChanges$?.unsubscribe();
   }
}
