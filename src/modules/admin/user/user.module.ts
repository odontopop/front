import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminUserComponent} from './user.component';
import {CommonModules} from '../../../shared/modules/common-modules/common-modules.module';

@NgModule({
   declarations: [
      AdminUserComponent
   ],
   imports: [
      CommonModule,
      CommonModules,
   ],
   providers: [],
})
export class AdminUserModule {
}
