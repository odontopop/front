import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminContractComponent} from './contract.component';
import {CommonModules} from '../../../shared/modules/common-modules/common-modules.module';

@NgModule({
   declarations: [
      AdminContractComponent
   ],
   imports: [
      CommonModule,
      CommonModules,
   ],
   providers: [],
})
export class AdminContractModule {
}
