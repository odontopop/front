import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {faEye} from '@fortawesome/free-solid-svg-icons';
import {MessagesService} from '../../../shared/services/messages.service';
import {FormsService} from '../../../shared/services/forms.service';

@Component({
   selector: 'app-admin-contract',
   templateUrl: './contract.component.html',
   styleUrls: ['./contract.component.scss']
})
export class AdminContractComponent {
   isLoading = false;

   faEye = faEye;

   contractModalRef: NgbModalRef;
   contractsToExpire: any = {
      data: [
         {
            id: '1',
            name: 'Apple',
            cnpj: '43488041000181',
            checked: true,
            contracts: [
               {
                  id: '1',
                  code: '037563',
                  dateExpire: '2020-09-01',
                  customer: {
                     id: '1',
                     name: 'Cliente 1',
                     cpf: '14654398612',
                  },
                  plan: {
                     id: '1',
                     name: 'Plano Basic A',
                     validityMonths: 3,
                  },
               }
            ],
         },
         {
            id: '3',
            name: 'Microsoft',
            cnpj: '25941315000164',
            checked: true,
            contracts: [
               {
                  id: '1',
                  code: '042581',
                  dateExpire: '2020-09-05',
                  customer: {
                     id: '1',
                     name: 'Cliente 1',
                     cpf: '14654398612',
                  },
                  plan: {
                     id: '1',
                     name: 'Plano Basic A',
                     validityMonths: 3,
                  },
               },
               {
                  id: '2',
                  code: '082582',
                  dateExpire: '2020-09-10',
                  customer: {
                     id: '1',
                     name: 'Cliente 1',
                     cpf: '14654398612',
                  },
                  plan: {
                     id: '1',
                     name: 'Plano Basic A',
                     validityMonths: 3,
                  },
               }
            ],
         }
      ]
   };
   sellerDetail: any = {};

   constructor(
      private modalService: NgbModal,
      private fb: FormBuilder,
      private msgService: MessagesService,
      private formsService: FormsService,
   ) {
   }

   openModalContractToExpire(modalContent, seller = null) {
      this.sellerDetail = seller;
      this.contractModalRef = this.modalService.open(modalContent);
   }

   save() {
   }
}
