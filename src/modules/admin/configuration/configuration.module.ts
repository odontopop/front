import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminConfigurationComponent} from './configuration.component';
import {CommonModules} from '../../../shared/modules/common-modules/common-modules.module';

@NgModule({
   declarations: [
      AdminConfigurationComponent
   ],
   imports: [
      CommonModule,
      CommonModules,
   ],
   providers: [],
})
export class AdminConfigurationModule {
}
