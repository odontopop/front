import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {faEdit, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {MessagesService} from '../../../shared/services/messages.service';
import {FormsService} from '../../../shared/services/forms.service';

@Component({
   selector: 'app-admin-configuration',
   templateUrl: './configuration.component.html',
   styleUrls: ['./configuration.component.scss']
})
export class AdminConfigurationComponent {
   isLoading = false;
   faEdit = faEdit;
   faPlus = faPlusCircle;

   configurationModalRef: NgbModalRef;
   configurationFormGroup: FormGroup;
   configurations: any = {
      data: [
         {
            id: '1',
            name: 'Bruno Silva',
            mail: 'bruno@gmail.com',
            cpf: '09480560020',
            active: true,
            sellers: [
               {
                  id: '1',
                  name: 'Apple',
                  cnpj: '43488041000181',
                  checked: true,
               },
               {
                  id: '2',
                  name: 'Google',
                  cnpj: '58869049000165',
                  active: false,
               },
               {
                  id: '3',
                  name: 'Microsoft',
                  cnpj: '25941315000164',
                  checked: true,
               }
            ],
         },
      ]
   };

   constructor(
      private modalService: NgbModal,
      private fb: FormBuilder,
      private msgService: MessagesService,
      private formsService: FormsService,
   ) {
   }

   openModalConfiguration(modalContent, configuration = null) {
      this.configurationFormGroup = this.fb.group({
         id: this.fb.control(configuration ? configuration.id : null),
         name: this.fb.control(configuration ? configuration.name : null, Validators.required),
         mail: this.fb.control(configuration ? configuration.mail : null, Validators.required),
         password: this.fb.control(configuration ? configuration.password : null, Validators.compose(configuration ? [] : [Validators.required])),
         cpf: this.fb.control(configuration ? configuration.cpf : null, Validators.required),
         active: this.fb.control(configuration ? configuration.active : true),
         sellers: this.builderSellerFormArray(configuration ? configuration.sellers : []),
      });

      this.configurationModalRef = this.modalService.open(modalContent);
   }

   builderSellerFormArray(sellers = []) {
      return this.fb.array(
         sellers.map((seller: any) => this.fb.group({
            id: this.fb.control(seller ? seller.id : null),
            name: this.fb.control(seller ? seller.name : null),
            cnpj: this.fb.control(seller ? seller.cnpj : null),
            checked: this.fb.control(seller ? seller.checked : null),
         }))
      );
   }

   save() {
      if (this.configurationFormGroup.invalid) {
         this.formsService.validateAllFormFields(this.configurationFormGroup);
         this.msgService.validateForm();
      } else {
         this.configurationModalRef.close();
      }
   }
}
