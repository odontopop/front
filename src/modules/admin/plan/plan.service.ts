import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
   providedIn: 'root',
})
export class PlanService {
   constructor(private http: HttpClient) {}

   getManyPlan(): any {
      return this.http.get(`${environment.baseURL}/plan`).toPromise();
   }

   save(plan: any) {
      if (plan.id === null) {
         delete plan.id;
         return this.http.post(`${environment.baseURL}/plan`, plan).toPromise();
      } else {
         return this.http.put(`${environment.baseURL}/plan`, plan).toPromise();
      }
   }
}
