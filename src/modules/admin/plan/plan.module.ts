import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminPlanComponent } from './plan.component';
import { CommonModules } from '../../../shared/modules/common-modules/common-modules.module';
import { PlanModalSaveModule } from 'src/components/admin/plan-modal-save/plan-modal-save.module';

@NgModule({
   declarations: [AdminPlanComponent],
   imports: [CommonModule, CommonModules, PlanModalSaveModule],
   providers: [],
})
export class AdminPlanModule {}
