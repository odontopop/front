import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { faEdit, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { PlanModalSaveComponent } from '../../../components/admin/plan-modal-save/plan.modal-save.component';
import { PlanService } from './plan.service';

@Component({
   selector: 'app-admin-plan',
   templateUrl: './plan.component.html',
   styleUrls: ['./plan.component.scss'],
})
export class AdminPlanComponent implements OnInit {
   @ViewChild('planModalSave') planModalSave: PlanModalSaveComponent;

   isLoading = false;
   faEdit = faEdit;
   faPlus = faPlusCircle;
   planModalRef: NgbModalRef;
   planFormGroup: FormGroup;
   plans: any[] = [];

   constructor(private planService: PlanService) {}

   openModalPlan(plan = null) {
      this.planModalSave.openModalPlan(plan);
   }

   async getManyPlan() {
      const { data } = await this.planService.getManyPlan();
      this.plans = data;
   }

   callBackOnSavePlan(success) {
      if (success) {
         this.getManyPlan();
      }
   }

   ngOnInit() {
      this.getManyPlan();
   }
}
