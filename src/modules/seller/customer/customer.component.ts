import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {faEdit, faFilePdf, faPlusCircle} from '@fortawesome/free-solid-svg-icons';
import {MessagesService} from '../../../shared/services/messages.service';
import {FormsService} from '../../../shared/services/forms.service';
import {CustomerService} from './customer.service';
import {PlanService} from '../../admin/plan/plan.service';

@Component({
   selector: 'app-seller-customer',
   templateUrl: './customer.component.html',
   styleUrls: ['./customer.component.scss']
})
export class SellerCustomerComponent implements OnInit {
   paginationSize = 30;
   currentPage = 1;
   isLoading = false;
   faEdit = faEdit;
   faPlus = faPlusCircle;
   faFilePdf = faFilePdf;

   customerModalRef: NgbModalRef;
   customerFormGroup: FormGroup;
   customers: any = {
      customer: [],
      total: 0
   };

   contractFormGroup: FormGroup;
   plans: any[] = [];

   reportCustomerParamsFormGroup: FormGroup;
   reportCustomerParamsModalRef: NgbModalRef;

   constructor(
      private modalService: NgbModal,
      private fb: FormBuilder,
      private msgService: MessagesService,
      private formsService: FormsService,
      private customerService: CustomerService,
      private planService: PlanService,
   ) {
   }

   openModalCustomer(modalContent, customer = null) {
      let dateSignature = null;

      if (customer) {
         const dateSignatureSplit = customer.customerPlan.dateSignature.split('-');
         dateSignature = `${dateSignatureSplit[2]}/${dateSignatureSplit[1]}/${dateSignatureSplit[0]}`;
      }

      this.customerFormGroup = this.fb.group({
         id: this.fb.control(customer ? customer.id : null),
         name: this.fb.control(customer ? customer.name : null, Validators.required),
         cpf: this.fb.control(customer ? customer.cpf : null, Validators.required),
         planId: this.fb.control(customer ? customer.customerPlan.plan.id : null, Validators.required),
         dateSignature: this.fb.control(dateSignature, Validators.required),
         loyaltyCard: this.fb.control(customer ? customer.customerPlan.loyaltyCard : false, Validators.required),
         active: this.fb.control(customer ? customer.active : true, Validators.required),
      });

      this.customerModalRef = this.modalService.open(modalContent, {size: 'md'});
   }

   async save() {
      if (this.customerFormGroup.invalid) {
         this.formsService.validateAllFormFields(this.customerFormGroup);
         this.msgService.validateForm();
      } else {
         try {
            this.isLoading = true;
            const dateSignatureSplit = this.customerFormGroup.get('dateSignature').value.split('/');

            await this.customerService.save({
               ...this.customerFormGroup.value,
               dateSignature: `${dateSignatureSplit[2]}-${dateSignatureSplit[1]}-${dateSignatureSplit[0]}`
            });
            this.msgService.success();
            this.customerModalRef.close();
            this.getCustomers();
         } catch (error) {
            this.msgService.error();
         } finally {
            this.isLoading = false;
         }
      }
   }

   async getCustomers() {
      try {
         this.isLoading = true;
         const {data} = await this.customerService.getManyCustomers(this.currentPage);
         this.customers = data;
      } catch (error) {
         this.msgService.error();
      } finally {
         this.isLoading = false;
      }
   }

   async getPlans() {
      try {
         this.isLoading = true;
         const {data} = await this.planService.getManyPlan();
         this.plans = data;
      } catch (error) {
         this.msgService.error();
      } finally {
         this.isLoading = false;
      }
   }

   async openPDFReportCustomers() {
      try {
         this.isLoading = true;

         const dateStartSplit = this.reportCustomerParamsFormGroup.get('dateStart').value.split('/');
         const dateStart = `${dateStartSplit[2]}-${dateStartSplit[1]}-${dateStartSplit[0]}`;
         const dateEndSplit = this.reportCustomerParamsFormGroup.get('dateEnd').value.split('/');
         const dateEnd = `${dateEndSplit[2]}-${dateEndSplit[1]}-${dateEndSplit[0]}`;
         const data: any = await this.customerService.getReportPDFCustomerLink(dateStart, dateEnd, this.reportCustomerParamsFormGroup.get('detailed').value);
         this.reportCustomerParamsModalRef.close();

         window.open(data.data, '_blank');
      } catch (error) {
         this.msgService.error();
      } finally {
         this.isLoading = false;
      }
   }

   openModalReportCustomer(modalContent) {
      this.reportCustomerParamsFormGroup = this.fb.group({
         dateStart: this.fb.control('', Validators.required),
         dateEnd: this.fb.control('', Validators.required),
         detailed: this.fb.control(false, Validators.required),
      });
      this.reportCustomerParamsModalRef = this.modalService.open(modalContent);
   }

   pageChange(pageNumber) {
      this.currentPage = pageNumber;
      this.getCustomers();
   }

   ngOnInit() {
      this.getCustomers();
      this.getPlans();
   }
}
