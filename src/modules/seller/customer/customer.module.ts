import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SellerCustomerComponent} from './customer.component';
import {CommonModules} from '../../../shared/modules/common-modules/common-modules.module';

@NgModule({
   declarations: [
      SellerCustomerComponent
   ],
   imports: [
      CommonModule,
      CommonModules,
   ],
   providers: [],
})
export class SellerCustomerModule {
}
