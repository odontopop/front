import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
   providedIn: 'root',
})
export class CustomerService {
   constructor(private http: HttpClient) {
   }

   getManyCustomers(page): any {
      return this.http.get(`${environment.baseURL}/customer?page=${page}`).toPromise();
   }

   save(customer: any) {
      if (customer.id === null) {
         delete customer.id;
         return this.http
            .post(`${environment.baseURL}/customer`, customer)
            .toPromise();
      } else {
         return this.http
            .put(`${environment.baseURL}/customer`, customer)
            .toPromise();
      }
   }

   getReportPDFCustomerLink(dateStart: string, dateEnd: string, detailed: boolean) {
      return this.http.get(`${environment.baseURL}/customer/report/list${detailed ? 'Detail' : ''}?dateStart=${dateStart}&dateEnd=${dateEnd}`).toPromise();
   }
}
