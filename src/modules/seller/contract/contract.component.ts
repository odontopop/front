import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
   faEdit,
   faPlusCircle,
   faSyncAlt,
} from '@fortawesome/free-solid-svg-icons';
import { MessagesService } from '../../../shared/services/messages.service';
import { FormsService } from '../../../shared/services/forms.service';

@Component({
   selector: 'app-seller-contract',
   templateUrl: './contract.component.html',
   styleUrls: ['./contract.component.scss'],
})
export class SellerContractComponent {
   isLoading = false;
   faEdit = faEdit;
   faPlus = faPlusCircle;
   faSync = faSyncAlt;

   contractModalRef: NgbModalRef;
   contractFormGroup: FormGroup;
   contracts: any = {
      data: [
         {
            id: '1',
            name: 'Cliente 1',
            mail: 'cliente1@gmail.com',
            cpf: '14654398612',
            contracts: [
               {
                  id: '1',
                  code: '042581',
                  dateExpire: '2020-09-05',
                  plan: {
                     id: '1',
                     name: 'Plano Basic A',
                     validityMonths: 3,
                  },
               },
            ],
         },
      ],
   };

   // contractFormGroup: FormGroup;
   // plans: any = {
   //    data: [
   //       {
   //          id: '1',
   //          name: 'Plano Basic A',
   //          validityMonths: 3,
   //       },
   //       {
   //          id: '2',
   //          name: 'Plano Premium A',
   //          validityMonths: 12,
   //       }
   //    ]
   // };

   // constructor(
   //    private modalService: NgbModal,
   //    private fb: FormBuilder,
   //    private msgService: MessagesService,
   //    private formsService: FormsService,
   // ) {
   // }

   // openModalContract(modalContent, contract = null) {
   //    this.contractFormGroup = this.fb.group({
   //       id: this.fb.control(contract ? contract.id : null),
   //       name: this.fb.control(contract ? contract.name : null, Validators.required),
   //       mail: this.fb.control(contract ? contract.mail : null, Validators.required),
   //       cpf: this.fb.control(contract ? contract.cpf : null, Validators.required),
   //       contracts: this.builderContractsFormArray(contract ? contract.contracts : []),
   //    });

   //    this.contractModalRef = this.modalService.open(modalContent);
   // }

   // builderContractsFormArray(contracts = []) {
   //    return this.fb.array(
   //       contracts.map((contract: any) => this.fb.group({
   //          id: this.fb.control(contract ? contract.id : null),
   //          code: this.fb.control(contract ? contract.code : null),
   //          dateExpire: this.fb.control(contract ? contract.dateExpire : null),
   //          plan: this.fb.group({
   //             id: this.fb.control(contract ? contract.plan.id : null),
   //             name: this.fb.control(contract ? contract.plan.name : null),
   //             validityMonths: this.fb.control(contract ? contract.plan.validityMonths : null),
   //          }),
   //       }))
   //    );
   // }

   // openModalContract(modalContent, contract = null) {
   //    this.contractFormGroup = this.fb.group({
   //       id: this.fb.control(contract ? contract.id : null),
   //       code: this.fb.control(contract ? contract.code : null),
   //       dateExpire: this.fb.control(contract ? contract.dateExpire : null),
   //       plan: this.fb.group({
   //          id: this.fb.control(contract ? contract.plan.id : null),
   //          name: this.fb.control(contract ? contract.plan.name : null),
   //          validityMonths: this.fb.control(contract ? contract.plan.validityMonths : null),
   //       }),
   //    });

   //    this.modalService.open(modalContent);
   // }

   // numericOnly(event): boolean {
   //    const patt = /^([0-9])$/;
   //    const result = patt.test(event.key);
   //    return result;
   // }

   // save() {
   //    if (this.contractFormGroup.invalid) {
   //       this.formsService.validateAllFormFields(this.contractFormGroup);
   //       this.msgService.validateForm();
   //    } else {
   //       this.contractModalRef.close();
   //    }
   // }
}
