import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SellerContractComponent} from './contract.component';
import {CommonModules} from '../../../shared/modules/common-modules/common-modules.module';

@NgModule({
   declarations: [
      SellerContractComponent
   ],
   imports: [
      CommonModule,
      CommonModules,
   ],
   providers: [],
})
export class SellerContractModule {
}
