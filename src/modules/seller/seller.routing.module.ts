import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SellerComponent} from './seller.component';
import {SellerCustomerComponent} from './customer/customer.component';
import {SellerContractComponent} from './contract/contract.component';

const routes: Routes = [
   {
      path: '',
      component: SellerComponent,
      children: [
         {
            path: 'clientes',
            component: SellerCustomerComponent,
         },
         {
            path: 'contratos',
            component: SellerContractComponent,
         },
      ]
   },
];

@NgModule({
   imports: [RouterModule.forChild(routes)],
   exports: [RouterModule]
})
export class SellerRoutingModule {
}
