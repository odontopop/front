import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';

import {SellerRoutingModule} from './seller.routing.module';
import {SellerNavbarModule} from '../../components/seller/navbar/navbar.module';
import {SellerComponent} from './seller.component';
import {SellerCustomerModule} from './customer/customer.module';
import {SellerContractModule} from './contract/contract.module';

@NgModule({
   declarations: [SellerComponent],
   imports: [
      CommonModule,
      NgbCollapseModule,

      SellerNavbarModule,
      SellerCustomerModule,
      SellerContractModule,
      SellerRoutingModule,
   ],
   providers: [],
})
export class SellerModule {
}
