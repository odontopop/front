import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthPasswordRecoverComponent} from './password-recover.component';
import {RouterModule} from '@angular/router';

@NgModule({
   declarations: [
      AuthPasswordRecoverComponent
   ],
   imports: [
      CommonModule,
      RouterModule,
   ],
   providers: [],
})
export class AuthPasswordRecoverModule {
}
