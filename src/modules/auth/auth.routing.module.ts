import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AuthComponent} from './auth.component';
import {AuthLoginComponent} from './login/login.component';
import {AuthPasswordRecoverComponent} from './password-recover/password-recover.component';

const routes: Routes = [
   {
      path: '',
      component: AuthComponent,
      children: [
         {
            path: '',
            component: AuthLoginComponent,
         },
         {
            path: 'recuperar-senha',
            component: AuthPasswordRecoverComponent,
         },
      ]
   },
];

@NgModule({
   imports: [RouterModule.forChild(routes)],
   exports: [RouterModule]
})
export class AuthRoutingModule {
}
