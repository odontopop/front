import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthRoutingModule} from './auth.routing.module';
import {AuthComponent} from './auth.component';
import {AuthLoginModule} from './login/login.module';
import {AuthPasswordRecoverModule} from './password-recover/password-recover.module';

@NgModule({
   declarations: [
      AuthComponent
   ],
   imports: [
      CommonModule,

      AuthLoginModule,
      AuthPasswordRecoverModule,
      AuthRoutingModule
   ],
   providers: [],
})
export class AuthModule {
}
