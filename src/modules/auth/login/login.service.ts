import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
   providedIn: 'root',
})
export class LoginService {
   constructor(private http: HttpClient) {}

   login(userData): any {
      return this.http
         .post(`${environment.baseURL}/users/login`, userData)
         .toPromise();
   }
}
