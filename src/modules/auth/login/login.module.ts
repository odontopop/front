import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthLoginComponent} from './login.component';
import {CommonModules} from '../../../shared/modules/common-modules/common-modules.module';

@NgModule({
   declarations: [
      AuthLoginComponent
   ],
   imports: [
      CommonModule,
      CommonModules
   ],
   providers: [],
})
export class AuthLoginModule {
}
