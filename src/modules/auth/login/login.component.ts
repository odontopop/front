import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';

import {MessagesService} from '../../../shared/services/messages.service';
import {FormsService} from '../../../shared/services/forms.service';
import {set} from '../../../shared/state/user/user.state';
import {LoginService} from './login.service';

@Component({
   selector: 'app-auth-login',
   templateUrl: './login.component.html',
   styleUrls: ['./login.component.scss'],
})
export class AuthLoginComponent implements OnInit {
   isLoading = false;
   authFormGroup: FormGroup;

   constructor(
      private fb: FormBuilder,
      private router: Router,
      private store: Store<any>,
      private msgService: MessagesService,
      private formsService: FormsService,
      private service: LoginService
   ) {
   }

   ngOnInit() {
      this.authFormGroup = this.fb.group({
         mail: this.fb.control(
            '',
            Validators.compose([Validators.required, Validators.email])
         ),
         password: this.fb.control(
            '',
            Validators.compose([Validators.required])
         ),
      });
   }

   async login() {
      if (this.authFormGroup.invalid) {
         this.formsService.validateAllFormFields(this.authFormGroup);
         this.msgService.validateForm();
      } else {
         try {
            this.isLoading = true;
            const {data} = await this.service.login({
               source: this.authFormGroup.get('mail').value,
               password: this.authFormGroup.get('password').value,
            });

            this.store.dispatch(
               set({
                  payload: {
                     name: data.name,
                     mail: data.mail,
                     nickname: data.nickname,
                     profile: data.profile,
                     token: data.token,
                     subsidiary: data.subsidiary,
                  },
               })
            );

            if (data?.profile?.toLowerCase() === 'admin') {
               console.log('dsd');
               this.router.navigateByUrl('/admin/programas');
            } else {
               this.router.navigateByUrl('/clinica/clientes');
            }
         } catch (err) {
            this.msgService.error();
         } finally {
            this.isLoading = false;
         }
      }
   }
}
