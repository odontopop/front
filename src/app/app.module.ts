import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DEFAULT_CURRENCY_CODE, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {ActionReducer, ActionReducerMap, MetaReducer, StoreModule, } from '@ngrx/store';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {localStorageSync} from 'ngrx-store-localstorage';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {reducer as userReducer} from '../shared/state/user/user.state';
import {AuthGuardService} from '../shared/guards/auth.guard';
import {AdminGuardService} from '../shared/guards/admin.guard';
import {LoginGuardService} from '../shared/guards/login.guard';
import {SellerGuardService} from '../shared/guards/seller.guard';
import {TokenInterceptor} from '../shared/interceptors/token.interceptor';
import {SubsidiaryInterceptor} from '../shared/interceptors/subsidiary.interceptor';

const reducers: ActionReducerMap<any> = {
   appUserReducer: userReducer,
};

export function localStorageSyncReducer(
   reducer: ActionReducer<any>
): ActionReducer<any> {
   return localStorageSync({keys: ['appUserReducer']})(reducer);
}

const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

@NgModule({
   declarations: [AppComponent],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      NgbModule,
      ToastrModule.forRoot({
         timeOut: 8000,
         positionClass: 'toast-top-right',
         preventDuplicates: true,
      }),
      HttpClientModule,
      LoadingBarHttpClientModule,
      StoreModule.forRoot(reducers, {metaReducers}),

      AppRoutingModule,

      FontAwesomeModule,
   ],
   providers: [
      AuthGuardService,
      AdminGuardService,
      LoginGuardService,
      SellerGuardService,

      {provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL'},
      {
         provide: HTTP_INTERCEPTORS,
         useClass: TokenInterceptor,
         multi: true,
      },
      {
         provide: HTTP_INTERCEPTORS,
         useClass: SubsidiaryInterceptor,
         multi: true,
      },
   ],
   bootstrap: [AppComponent],
})
export class AppModule {
}
