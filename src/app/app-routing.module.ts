import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuardService } from 'src/shared/guards/admin.guard';
import { SellerGuardService } from 'src/shared/guards/seller.guard';
import { AuthGuardService } from 'src/shared/guards/auth.guard';
import { LoginGuardService } from 'src/shared/guards/login.guard';

const routes: Routes = [
   {
      path: 'pagina-nao-encontrada',
      loadChildren: () =>
         import('../modules/not-found/not-found.module').then(
            (m) => m.NotFoundModule
         ),
   },
   {
      path: 'login',
      canActivate: [LoginGuardService],
      loadChildren: () =>
         import('../modules/auth/auth.module').then((m) => m.AuthModule),
   },
   {
      path: 'admin',
      canActivate: [AuthGuardService, AdminGuardService],
      loadChildren: () =>
         import('../modules/admin/admin.module').then((m) => m.AdminModule),
   },
   {
      path: 'clinica',
      canActivate: [AuthGuardService, SellerGuardService],
      loadChildren: () =>
         import('../modules/seller/seller.module').then((m) => m.SellerModule),
   },

   {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full',
   },

   { path: '**', redirectTo: '/pagina-nao-encontrada' },
];

@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule],
})
export class AppRoutingModule {}
