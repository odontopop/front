import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbCollapseModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {SellerNavbarComponent} from './navbar.component';

@NgModule({
   declarations: [
      SellerNavbarComponent
   ],
   imports: [
      CommonModule,
      RouterModule,
      NgbCollapseModule,
      NgbDropdownModule,
      FontAwesomeModule,
   ],
   providers: [],
   exports: [
      SellerNavbarComponent
   ]
})
export class SellerNavbarModule {
}
