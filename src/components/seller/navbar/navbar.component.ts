import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

import NAVBAR_DATA from './navbar-data';
import { reset } from '../../../shared/state/user/user.state';

@Component({
   selector: 'app-seller-navbar',
   templateUrl: './navbar.component.html',
   styleUrls: ['./navbar.component.scss'],
})
export class SellerNavbarComponent implements OnInit {
   user$: Observable<any>;

   faSignOutAlt = faSignOutAlt;

   navbarCollapsed = true;
   NAVBAR_DATA = NAVBAR_DATA;
   currentRoute: '';

   constructor(private router: Router, private store: Store<any>) {
      this.router.events.subscribe(
         (route: any) => (this.currentRoute = route.url)
      );
   }

   ngOnInit() {
      this.user$ = this.store.pipe(select('appUserReducer'));
   }

   getFirstName(name: string) {
      return name?.length > 0 ? name.split(' ')[0].toUpperCase() : '';
   }

   signOut() {
      this.store.dispatch(reset());
      this.router.navigateByUrl('/login');
   }
}
