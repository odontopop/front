export default [
   {
      text: 'Programas',
      route: 'programas',
   },
   {
      text: 'Clínicas Parceiras',
      route: 'clinicas',
   },
   {
      text: 'Usuários',
      route: 'usuarios',
   },
   // {
   //    text: 'Contratos',
   //    route: 'contratos',
   //    badge: 3,
   // },
   // {
   //    text: 'Configurações',
   //    route: 'configuracoes',
   // },
];
