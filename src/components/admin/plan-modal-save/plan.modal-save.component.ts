import { Component, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlanService } from '../../../modules/admin/plan/plan.service';
import { FormsService } from '../../../shared/services/forms.service';
import { MessagesService } from '../../../shared/services/messages.service';

@Component({
   selector: 'app-plan-modal-save',
   templateUrl: './plan-modal-save.component.html',
   styleUrls: ['./plan-modal-save.component.scss'],
})
export class PlanModalSaveComponent {
   @ViewChild('modalPlanSave') modalPlanSave: any;
   @Output() onSave: EventEmitter<boolean> = new EventEmitter();

   protected isLoading = false;
   protected planModalRef: NgbModalRef;
   protected planFormGroup: FormGroup;
   protected plans: any[] = [];

   constructor(
      private modalService: NgbModal,
      private fb: FormBuilder,
      private msgService: MessagesService,
      private formsService: FormsService,
      private planService: PlanService
   ) {}

   numericOnly(event): boolean {
      const patt = /^([0-9])$/;
      const result = patt.test(event.key);
      return result;
   }

   openModalPlan(plan = null) {
      this.planFormGroup = this.fb.group({
         id: this.fb.control(plan ? plan.id : null),
         name: this.fb.control(plan ? plan.name : null, Validators.required),
         value: this.fb.control(plan ? plan.value : null, Validators.required),
         percent: this.fb.control(
            plan ? plan.percent : null,
            Validators.required
         ),
         parcel: this.fb.control(
            plan ? plan.parcel : null,
            Validators.required
         ),
      });

      this.planModalRef = this.modalService.open(this.modalPlanSave);
   }

   async save() {
      if (this.planFormGroup.invalid) {
         this.formsService.validateAllFormFields(this.planFormGroup);
         this.msgService.validateForm();
      } else {
         try {
            this.isLoading = true;
            await this.planService.save(this.planFormGroup.value);
            this.onSave.emit(true);
            this.msgService.success();
            this.planModalRef.close();
         } catch (err) {
            this.msgService.error();
         } finally {
            this.isLoading = false;
         }
      }
   }
}
