import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanModalSaveComponent } from './plan.modal-save.component';
import { PlanModalSaveService } from './plan-modal-save.service';
import { CommonModules } from 'src/shared/modules/common-modules/common-modules.module';

@NgModule({
   declarations: [PlanModalSaveComponent],
   imports: [CommonModule, CommonModules],
   exports: [PlanModalSaveComponent],
   providers: [PlanModalSaveService],
})
export class PlanModalSaveModule {}
