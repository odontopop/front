import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
   providedIn: 'root',
})
export class MessagesService {
   constructor(protected toast: ToastrService) {}

   error(message = null) {
      this.toast.error(
         message
            ? message
            : 'Ooops.. Não conseguimos completar sua requisição, tente novamente.'
      );
   }

   success(message = null, disableTimeout = false) {
      this.toast.success(
         message ? message : 'Operação realizada com sucesso.',
         '',
         { disableTimeOut: disableTimeout }
      );
   }

   validateForm(message = null) {
      this.toast.warning(
         message ? message : 'Preencha os campos destacados para prosseguir.'
      );
   }
}
