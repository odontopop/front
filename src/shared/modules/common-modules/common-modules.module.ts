import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
   NgbAccordionModule,
   NgbAlertModule,
   NgbCarouselModule,
   NgbCollapseModule,
   NgbDropdownModule,
   NgbModalModule,
   NgbModule,
   NgbPaginationModule,
   NgbRatingModule,
   NgbTabsetModule,
   NgbTooltipModule,
} from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';
import { LaddaModule } from 'angular2-ladda';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxCurrencyModule } from 'ngx-currency';

import { CpfCnpjFormatPipe } from '../../pipes/cpf-cnpj-format.pipe';

@NgModule({
   declarations: [CpfCnpjFormatPipe],
   imports: [
      CommonModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule,
      NgxMaskModule.forRoot(),
      LaddaModule.forRoot({
         style: 'expand-right',
         spinnerSize: 26,
         spinnerColor: '#000',
         spinnerLines: 12,
      }),
      NgxCurrencyModule,
   ],
   exports: [
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      NgxMaskModule,
      NgbModule,
      NgbAlertModule,
      NgbAccordionModule,
      NgbCollapseModule,
      NgbCarouselModule,
      NgbRatingModule,
      NgbPaginationModule,
      NgbTabsetModule,
      NgbModalModule,
      NgbDropdownModule,
      NgbTooltipModule,
      LaddaModule,
      NgxTrimDirectiveModule,
      FontAwesomeModule,
      NgxCurrencyModule,

      CpfCnpjFormatPipe,
   ],
})
export class CommonModules {}
