import { createAction, props } from '@ngrx/store';

export const USER_FLAG_STORAGE = 'appUserReducer';
const INITIAL_STATE = {};
enum ActionTypes {
   Set = 'Set',
   Get = 'Get',
   Reset = 'Reset',
}

export const set = createAction(ActionTypes.Set, props<{ payload: any }>());

export const get = createAction(ActionTypes.Get);

export const reset = createAction(ActionTypes.Reset);

export function reducer(state = INITIAL_STATE, action: any) {
   switch (action.type) {
      case ActionTypes.Set: {
         state = action.payload;
         localStorage.setItem(USER_FLAG_STORAGE, JSON.stringify(state));
         return state;
      }

      case ActionTypes.Reset: {
         return INITIAL_STATE;
      }

      default:
         const local = JSON.parse(localStorage.getItem(USER_FLAG_STORAGE));
         return local ? local : state;
   }
}
