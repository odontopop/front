import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
   name: 'cpfCnpjFormatPipe',
})
export class CpfCnpjFormatPipe implements PipeTransform {
   transform(text: string): string {
      const cnpjCpf = text.replace(/\D/g, '');

      if (cnpjCpf.length === 11) {
         return cnpjCpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '\$1.\$2.\$3-\$4');
      }

      return cnpjCpf.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '\$1.\$2.\$3/\$4-\$5');
   }
}
