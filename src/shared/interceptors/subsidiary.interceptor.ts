import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {USER_FLAG_STORAGE} from '../state/user/user.state';

@Injectable()
export class SubsidiaryInterceptor implements HttpInterceptor {
   intercept(req: HttpRequest<any>, next: HttpHandler) {
      const userStorage = JSON.parse(localStorage.getItem(USER_FLAG_STORAGE));
      const objHeader: any = {};

      if (userStorage?.subsidiary?.id) {
         objHeader.subsidiaryId = userStorage?.subsidiary?.id;
      }

      return next.handle(
         req.clone({
            setHeaders: objHeader,
         })
      );
   }
}
