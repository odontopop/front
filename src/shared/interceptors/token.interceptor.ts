import {
   HttpInterceptor,
   HttpRequest,
   HttpHandler,
   HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { USER_FLAG_STORAGE } from '../state/user/user.state';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
   intercept(req: HttpRequest<any>, next: HttpHandler) {
      const userStorage = JSON.parse(localStorage.getItem(USER_FLAG_STORAGE));
      const objHeader: any = {};

      if (userStorage?.token) {
         objHeader.Authorization = `Bearer ${userStorage?.token}`;
      }

      return next.handle(
         req.clone({
            setHeaders: objHeader,
         })
      );
   }
}
