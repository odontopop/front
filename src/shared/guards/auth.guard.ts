import { Injectable } from '@angular/core';
import {
   CanActivate,
   Router,
   ActivatedRouteSnapshot,
   RouterStateSnapshot,
} from '@angular/router';
import { USER_FLAG_STORAGE } from '../state/user/user.state';

@Injectable()
export class AuthGuardService implements CanActivate {
   constructor(private router: Router) {}

   canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
   ): boolean {
      const userSession = JSON.parse(localStorage.getItem(USER_FLAG_STORAGE));
      if (userSession?.token) {
         return true;
      }

      this.router.navigateByUrl('/login');
      return false;
   }
}
