import { Injectable } from '@angular/core';
import {
   CanActivate,
   Router,
   ActivatedRouteSnapshot,
   RouterStateSnapshot,
} from '@angular/router';
import { USER_FLAG_STORAGE } from '../state/user/user.state';

@Injectable()
export class AdminGuardService implements CanActivate {
   constructor(private router: Router) {}

   canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
   ): boolean {
      const { profile } = JSON.parse(localStorage.getItem(USER_FLAG_STORAGE));

      if (profile?.toLowerCase() !== 'admin') {
         this.router.navigateByUrl('/pagina-nao-encontrada');
         return false;
      }

      return true;
   }
}
