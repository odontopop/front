import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, } from '@angular/router';
import {USER_FLAG_STORAGE} from '../state/user/user.state';

@Injectable()
export class SellerGuardService implements CanActivate {
   constructor(private router: Router) {
   }

   canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
   ): boolean {
      const userStorage = JSON.parse(localStorage.getItem(USER_FLAG_STORAGE));

      if (userStorage?.profile.toLowerCase() !== 'user') {
         this.router.navigateByUrl('/pagina-nao-encontrada');
         return false;
      }

      return true;
   }
}
